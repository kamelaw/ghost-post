from django.db import models
from django.utils import timezone


# Create your models here.
# one model that represents both boasts and roasts
class Post(models.Model):
    isBoast = models.BooleanField()   # null=True, default=True
    text = models.CharField(max_length=280)
    # at the beginning when we start they'll be at 0
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now)

    # And then just call the method in your template
    def total(self):
        total = self.likes + self.dislikes
        return total

    def vote_score(self):
        vote_score = self.likes - self.dislikes
        return vote_score

    # makes it more readable for admin panel
    def __str__(self):
        return self.text
