from django.shortcuts import render, redirect, reverse, HttpResponseRedirect
from ghost_app.models import Post
from ghost_app.forms import CreatePostForm
from ghost_app.forms import boast_roast

# Create your views here.


def index_view(request):
    # collection of all the posts
    posts = Post.objects.all().order_by('created_at').reverse()
    return render(request, 'index.html', {'posts': posts})


# upvoting. changes score & re-renders page. this is boasts
def like_view(request, post_id):
    # target a specific post & increase that score
    # get access to post we want to update
    # filter takes in keyword argument post_id
    # id of given post we want to update
    # has 5 fields with an id field that's already preset
    # id=post_id will give us back a queryset. that's what filter returns
    # queryset of the single post we want to interact with, but we want the object
    # inside the queryset so .first() returns the first thing in the query so can
    # interface with the object. (example) something inside of a list instead of the list
    post = Post.objects.filter(id=post_id).first()
    # we want to specifically change the likes on the post.
    # target with . notation. += increases it by 1 everytime
    post.likes += 1
    # need this to save it to db so it works.
    post.save()
    # return redirect('/')
    return HttpResponseRedirect(reverse('homepage'))


# downvoting. changes score & re-renders page. this is roasts
def dislike_view(request, post_id):
    post = Post.objects.filter(id=post_id).first()
    post.dislikes += 1
    post.save()
    # return redirect('/')
    return HttpResponseRedirect(reverse('homepage'))


# got help from April on this view
def votes_view(request):
    posts = sorted(Post.objects.all().order_by('created_at'), key=lambda a: a.likes - a.dislikes, reverse=True)
    return render(request, 'index.html', {'posts': posts})


# April helped me with this one!!
def boasts(request):
    posts = Post.objects.filter(isBoast='True').order_by('-created_at')
    return render(request, 'index.html', {'posts': posts})


# April helped me with this one
def roasts(request):
    posts = Post.objects.filter(isBoast='False').order_by('created_at')
    return render(request, 'index.html', {'posts': posts})


def create_post(request):
    context = {}
    if request.method == 'POST':
        form = CreatePostForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            new_item = Post.objects.create(
                text=data['text'],
                isBoast=data['isBoast']

            )
            return HttpResponseRedirect(reverse('homepage'))

    form = CreatePostForm
    context.update({'form': form})
    return render(
        request,
        "ghost_form.html",
        context
    )



