from django.contrib import admin
from ghost_app.models import Post

# Register your models here.
admin.site.register(Post)