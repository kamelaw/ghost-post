from django import forms
from ghost_app.models import Post
"""
class Post(models.Model):
    boast_roast = models.BooleanField()
    text = models.CharField(max_length=280)
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.text
"""
# Mai helped with the form but i changed the true and false because
# i think boast should be true and like
boast_roast = [
    (True, 'Boast'),
    (False, 'Roast'),

]


class CreatePostForm(forms.Form):
    text = forms.CharField(max_length=280)
    isBoast = forms.ChoiceField(
        label='Is roast:', choices=boast_roast, required=True)
